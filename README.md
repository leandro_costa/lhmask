# README #

Biblioteca com funções para formatação de dados.

CPF/CNPJ: Retorna no CPF ou CNPJ de Acordo com o Tamanho da Entrada.

### Exemplos

```js
import { formatarCep } from 'lhmask';

console.log(formatarCep('12345000'));  // Saída: 12.345-000

console.log(formatarCnpj('12123456000100'));  // Saída 12.123.456/0001-00

console.log(formatarCpf('12345678900'));  // Saída: 123.456.789-00

console.log(formatarCpfCnpj('12123456000100'));  // Saída 12.123.456/0001-00

console.log(formatarCpfCnpj('12345678900'));  // Saída: 123.456.789-00

console.log(formatarContaBancaria('12345'));  // Saída: 1234-5

console.log(formatarTelefone('88912341234'));  // Saída: (88)91234-1234.
console.log(formatarTelefone('8834001234'));  // Saída: (88)3400-1234.
console.log(formatarTelefone('08001234567'));  // Saída: 0800-123-4567

// Retorna no formado especificado pela string.
// possíveis valores: cep, cpf, cnpj, cpfcnpj, contabancaria.
console.log(formatarValor("12345", "contabancaria")); // Saída: 1234-5
```