import * as lhmask from "./lhmask";

const testes = {
  cep: {
    fnName: "formatarCep",
    valores: [["62800000", "62.800-000"]],
  },
  cnpj: {
    fnName: "formatarCnpj",
    valores: [["12123456000100", "12.123.456/0001-00"]],
  },
  cpf: {
    fnName: "formatarCpf",
    valores: [["12345678900", "123.456.789-00"]],
  },
  cpfcnpj: {
    fnName: "formatarCpfCnpj",
    valores: [
      ["12123456000100", "12.123.456/0001-00"],
      ["12345678900", "123.456.789-00"],
    ],
  },
  contabancaria: {
    fnName: "formatarContaBancaria",
    valores: [
      ["12345", "1234-5"],
      ["12345x", "12345-x"],
      ["12345X", "12345-X"],
    ],
  },
  telefone: {
    fnName: "formatarTelefone",
    valores: [
      ["88912341234", "(88)91234-1234"],
      ["8834001234", "(88)3400-1234"],
      ["08001234567", "0800-123-4567"],
    ],
  },
};

describe.each(Object.keys(testes).map((k) => [k]))(`%s`, (formato: string) => {
  const nomeFormato = formato as keyof typeof testes;
  const teste = testes[nomeFormato];
  const fnName = teste.fnName;
  const valores = teste.valores;

  test.each(valores.map((v) => [fnName, v[0], v[1]]))(`%s(%s) => %s)`, (fnName, v0, v1) => {
    const formatFunction = lhmask[fnName as keyof typeof lhmask] as Function;
    expect(formatFunction(v0)).toBe(v1);
    expect(lhmask.formatarValor(v0, formato)).toBe(v1);
  });
});
