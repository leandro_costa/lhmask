// Entrada: 12345000 Saída: 12.345-000
export function formatarCep(txt: string) {
  const soNumeros = `${txt}`.replace(/[^\d]/g, "");
  if (!soNumeros) return soNumeros;

  let cep = "";
  cep += soNumeros.slice(0, 2);
  if (soNumeros.length > 2) cep += "." + soNumeros.slice(2, 5);

  if (soNumeros.length > 5) cep += "-" + soNumeros.slice(5, 8);

  return cep;
}

// Entrada: 12345678000100 Saida: 12.345.678/0001-00
export function formatarCnpj(txt: string) {
  const soNumeros = `${txt}`.replace(/[^\d]/g, "");
  if (!soNumeros) return soNumeros;

  let cnpj = "";
  cnpj += soNumeros.slice(0, 2);

  if (soNumeros.length > 2) cnpj += "." + soNumeros.slice(2, 5);

  if (soNumeros.length > 5) cnpj += "." + soNumeros.slice(5, 8);

  if (soNumeros.length > 8) cnpj += "/" + soNumeros.slice(8, 12);

  if (soNumeros.length > 12) cnpj += "-" + soNumeros.slice(12, 14);

  return cnpj;
}

// Entrada: 12345678900 Saída: 123.456.789-00
export function formatarCpf(txt: string) {
  const soNumeros = `${txt}`.replace(/[^\d]/g, "");
  if (!soNumeros) return soNumeros;

  let cpf = "";
  cpf += soNumeros.slice(0, 3);

  if (soNumeros.length > 3) cpf += "." + soNumeros.slice(3, 6);

  if (soNumeros.length > 6) cpf += "." + soNumeros.slice(6, 9);

  if (soNumeros.length > 9) cpf += "-" + soNumeros.slice(9, 11);

  return cpf;
}

export function formatarCpfCnpj(txt: string) {
  const soNumeros = `${txt}`.replace(/[^\d]/g, "");
  if (soNumeros.length <= 11) {
    return formatarCpf(txt);
  } else {
    return formatarCnpj(txt);
  }
}

// Entrada: 12345 Saída: 1234-5
export function formatarContaBancaria(txt: string) {
  const soNumeros = `${txt}`.replace(/[^\dxX]/g, "");
  if (!soNumeros) return soNumeros;

  let conta = "";
  if (soNumeros.length > 1) {
    conta = soNumeros.slice(0, soNumeros.length - 1) + "-" + soNumeros[soNumeros.length - 1];
  } else {
    conta = soNumeros;
  }

  return conta;
}

export function formatarTelefone(txt: string) {
  const soNumeros = `${txt}`.replace(/[^\d]/g, "");
  if (!soNumeros) return soNumeros;

  let tel = "";
  if (soNumeros.length === 1) {
    if (+soNumeros[0] === 0) {
      tel = soNumeros;
    } else {
      tel = "(" + soNumeros;
    }
  } else if (soNumeros.length === 2) {
    if (+soNumeros[0] === 0 && +soNumeros[1] === 8) {
      tel = soNumeros;
    } else {
      tel = "(" + soNumeros;
    }
  } else if (soNumeros.length > 2) {
    if (+soNumeros[2] === 9) {
      // CELULAR
      tel = "(" + soNumeros.slice(0, 2) + ")";
      if (soNumeros.length > 2) tel += soNumeros.slice(2, 7);

      if (soNumeros.length > 7) tel += "-" + soNumeros.slice(7, 11);
    } else {
      // TELEFONE FIXO OU 0800
      if (+soNumeros[0] === 0 && +soNumeros[1] === 8) {
        // 0800
        tel = soNumeros.slice(0, 4);
        if (soNumeros.length > 4) tel += "-" + soNumeros.slice(4, 7);
        if (soNumeros.length > 7) tel += "-" + soNumeros.slice(7, 11);
      } else {
        tel = "(" + soNumeros.slice(0, 2) + ")";
        if (soNumeros.length > 2) tel += soNumeros.slice(2, 6);

        if (soNumeros.length > 6) tel += "-" + soNumeros.slice(6, 10);
      }
    }
  }

  return tel;
}

export function formatarValor(valor: string, formato: string) {
  switch (formato) {
    case "cep":
      return formatarCep(valor);
    case "cpf":
      return formatarCpf(valor);
    case "cnpj":
      return formatarCnpj(valor);
    case "cpfcnpj":
      return formatarCpfCnpj(valor);
    case "contabancaria":
      return formatarContaBancaria(valor);
    case "telefone":
      return formatarTelefone(valor);
    case "int":
      return parseInt(valor);
    case "float":
      return parseFloat(valor);
    default:
      return valor;
  }
}

export default {
  formatarCep,
  formatarCnpj,
  formatarCpf,
  formatarCpfCnpj,
  formatarContaBancaria,
  formatarTelefone,
  formatarValor,
};
